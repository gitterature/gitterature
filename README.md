# Gittérature

Gittérature – une initiative qui vise à, lister, inventorier documenter, répertorier, voire théoriser la littérature [Git](https://git-scm.com/).

## Répertoire

_Encore très préliminaire, cette liste pourra être subdivisée selon les différentes catégories que le collaborateur trouvera pertinentes._

- [Git Book](http://brendandawes.com/blog/gitbook) – Billet de blogue qui présente le projet de Brendan Dawes.
- [Cheminement textuel](https://www.quaternum.net/2019/12/09/cheminement-textuel/) – Un texte de création issu de l’enchaînement de messages Git, par Antoine Fauchié.
- [Piece 01](https://scolaire.loupbrun.ca/piece01/) – Une «pièce de théâtre», dont les dialogues correspondent aux messages de commits des différents personnages (avec signature GPG), par Louis-Olivier Brassard.
- [Abrüpt](https://abrupt.cc) – Une maison de publication indépendante qui propose des publications numériques, imprimées et DIY.
- [Gittérature](La Cyberpoétique) – Adjonction de la littérature aux potentialités de l'outil libre Git, saupoudrée de lignes Bash ([dépôt Git](https://gitlab.com/antilivre/gitterature)). Un projet signé Le Réseaü / Cyberpoétique / Abrüpt.
- [Enfer.txt](https://abrupt.cc/zap/rimbaud/), réécriture collaborative du poème <cite>Une saison en enfer</cite> d’Arthur Rimbaud.
  - Pour un commentaire critique sur l’œuvre et le sujet, voir [<cite>Pour une gittérature</cite>](https://shs.hal.science/hal-03962836v1) par Servanne Monjour et Nicolas Sauret.

## Licence

[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) – Domaine public.
